import sys
import subprocess
import re
import os
import logging
import platform

python_version = platform.python_version_tuple()


class loadaverages(object):
    def __init__(self, agent_config, internal_config, main_logger):
        self.agent_config = agent_config
        self.internal_config = internal_config
        self.main_logger = main_logger

    def run(self):
        self.main_logger.debug("load_averages || start")

        if sys.platform == "linux2":
            self.main_logger.debug("load_averages || linux2")

            try:
                self.main_logger.debug("load_averages || attempting to open loadavg")

                load_avg_proc = open("/proc/loadavg", "r")
                uptime = load_avg_proc.readlines()

            except IOError, e:
                self.main_logger.error("load_averages || exception: %s", traceback.format_exc())
                return False

            self.main_logger.debug("load_averages || open successful")

            load_avg_proc.close()
            uptime = uptime[0]

        elif sys.platform.find("freebsd") is not -1:
            self.main_logger.debug("load_averages || freebsd (uptime)")
            try:
                try:
                    self.main_logger.debug("load_averages || popen")
                    proc = subprocess.Popen(["uptime"], stdout=subprocess.PIPE, close_fds=True)
                    uptime = proc.communicate()[0]

                    if int(python_version[1]) >= 6:
                        try:
                            proc.kill()
                        except Exception, e:
                            self.main_logger.debug("load_averages || Process has already been killed")

                except Exception, e:
                    import traceback
                    self.main_logger.error("load_averages || exception: %s", traceback.format_exc())
                    return False

            finally:
                if int(python_version[1]) >= 6:
                    try:
                        proc.kill()
                    except Exception, e:
                        self.main_logger.debug("load_averages || Process has already been killed")

            self.main_logger.debug("load_averages || popen successful")

        elif sys.platform == "darwin":
            self.main_logger.debug("load_averages || darwin")
            try:
                try:
                    self.main_logger.debug("load_averages || popen")
                    proc = subprocess.Popen(["uptime"], stdout=subprocess.PIPE, close_fds=True)
                    uptime = proc.communicate()[0]

                    if int(python_version[1]) >= 6:
                        try:
                            proc.kill()
                        except Exception, e:
                            self.main_logger.debug("load_averages || Process has already been killed")

                except Exception, e:
                    import traceback
                    self.main_logger.error("load_averages || exception: %s", traceback.format_exc())
                    return False

            finally:
                if int(python_version[1]) >= 6:
                    try:
                        proc.kill()
                    except Exception, e:
                        self.main_logger.debug("load_averages || Process has already been killed")

            self.main_logger.debug("load_averages || popen successful")

        elif sys.platform.find("sunos") is not -1:
            self.main_logger.debug("load_averages || solaris (uptime)")
            try:
                try:
                    self.main_logger.debug("load_averages || popen")
                    proc = subprocess.Popen(["uptime"], stdout=subprocess.PIPE, close_fds=True)
                    uptime = proc.communicate()[0]

                    if int(python_version[1]) >= 6:
                        try:
                            proc.kill()
                        except Exception, e:
                            self.main_logger.debug("load_averages || Process has already been killed")

                except Exception, e:
                    import traceback
                    self.main_logger.error("load_averages || exception: %s", traceback.format_exc())
                    return False

            finally:
                if int(python_version[1]) >= 6:
                    try:
                        proc.kill()
                    except Exception, e:
                        self.main_logger.debug("load_averages || Process has already been killed")

            self.main_logger.debug("load_averages || popen successful")

        else:
            self.main_logger.debug("load_averages || Unsupported platform")
            return False

        self.main_logger.debug("load_averages || parsing")

        load_avgs = [res.replace(",", ".") for res in re.findall(r'([0-9]+[\.,]\d+)', uptime)]
        load_avgs = {"1": load_avgs[0], "5": load_avgs[1], "15": load_avgs[2]}

        self.main_logger.debug("load_averages || completed")
        return load_avgs
