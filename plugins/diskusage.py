import platform
import re
import subprocess
import logging
import logging.handlers

python_version = platform.python_version_tuple()


class diskusage(object):
    def __init__(self, agent_config, internal_config, main_logger):
        self.agent_config = agent_config
        self.internal_config = internal_config
        self.main_logger = main_logger

    def run(self):
        self.main_logger.debug("disk_usage_statistics || start")

        try:
            try:
                self.main_logger.debug("disk_usage_statistics || attempting Popen")
                proc = subprocess.Popen(["df", "-k"], stdout=subprocess.PIPE, close_fds=True)
                stats = proc.communicate()[0]

                if int(python_version[1]) >= 6:
                    try:
                        proc.kill()
                    except Exception, e:
                        self.main_logger.debug("disk_usage_statistics || Process has already been killed")

            except Exception, e:
                import traceback
                self.main_logger.error("disk_usage_statistics || exception: %s", traceback.format_exc())
                return False

        finally:
            if int(python_version[1]) >= 6:
                try:
                    proc.kill()
                except Exception, e:
                    self.main_logger.debug("disk_usage_statistics || Process has already been killed")

        self.main_logger.debug("disk_usage_statistics || Starting parsing of disk usage stats")

        volumes = stats.split("\n")
        self.main_logger.debug("disk_usage_statistics || parsing, split")

        volumes.pop(0)
        volumes.pop()

        self.main_logger.debug("disk_usage_statistics || parsing, pop")

        usage_data = {}

        reg_expression = re.compile(r'([0-9]+)')

        prev_volume = None
        vol_count = 0

        self.main_logger.debug("disk_usage_statistics || parsing, start loop")

        for v in volumes:
            self.main_logger.debug("disk_usage_statistics || parsing volume %s", v)

            v = v.split(None, 10)

            if len(v) is 1:
                prev_volume = v[0]
                continue

            if prev_volume is not None:
                v.insert(0, prev_volume)
                prev_volume = None

            vol_count = vol_count + 1

            if re.match(reg_expression, v[1]) is None or re.match(reg_expression, v[2]) is None or re.match(reg_expression, v[3]) is None:
                pass
            else:
                try:
                    v[2] = float(v[2]) / 1024 / 1024  # Used
                    v[3] = float(v[3]) / 1024 / 1024  # Avail
                except Exception, e:
                    self.main_logger.error("disk_usage_statistics || parsing, loop %s Used not present in data" % (repr(e),))

                usage_data["file_sys"] = v[0]
                usage_data["block_size"] = v[1]
                usage_data["used"] = v[2]
                usage_data["avail"] = v[3]
                usage_data["percentage"] = v[4]
                usage_data["mount_point"] = v[5]

        self.main_logger.debug("disk_usage_statistics || completed, returning")
        return usage_data
