import sys
import subprocess
import re
import logging
import platform

python_version = platform.python_version_tuple()


class iostats(object):
    def __init__(self, agent_config, internal_config, main_logger):
        self.agent_config = agent_config
        self.internal_config = internal_config
        self.main_logger = main_logger

    def run(self):
        self.main_logger.debug("io_stats || start")

        io_stats = {}

        if sys.platform == "linux2":
            self.main_logger.debug("io_stats || linux2")

            head_regex_pattern = re.compile(r'([%\\/\-\_a-zA-Z0-9]+)[\s+]?')
            item_regex_pattern = re.compile(r'^([a-zA-Z0-9\/]+)')
            value_regex_pattern = re.compile(r'\d+\.\d+')

            try:
                try:
                    proc = subprocess.Popen(["iostat", "-d", "1", "2", "-x", "-k"], stdout=subprocess.PIPE, close_fds=True)
                    stats = proc.communicate()[0]

                    if int(python_version[1]) >= 6:
                        try:
                            proc.kill()
                        except Exception, e:
                            self.main_logger.debug("io_stats || Process has already been killed ")

                    if stats is None or stats == "":
                        self.main_logger.error("io_stats || No device found. Looks like a VPS")
                    else:
                        recent_stats = stats.split("Device:")[2].split("\n")
                        header = recent_stats[0]
                        header_names = re.findall(head_regex_pattern, header)
                        device = None

                        for s_index in range(1, len(recent_stats)):
                            row = recent_stats[s_index]

                            if not row:
                                continue

                            device_match = re.match(item_regex_pattern, row)

                            if device_match is not None:
                                device = device_match.groups()[0]

                            values = re.findall(value_regex_pattern, row.replace(",", "."))

                            if not values:
                                continue

                            io_stats[device] = {}

                            for h_index in range(0, len(header_names)):
                                header_name = header_names[h_index]
                                io_stats[device][header_name] = values[h_index]

                except OSError, ex:
                    return False

                except Exception, ex:
                    import traceback
                    self.main_logger.error("io_stats || exception: %s", traceback.format_exc())
                    return False

            finally:
                if int(python_version[1]) >= 6:
                    try:
                        proc.kill()
                    except Exception, e:
                        self.main_logger.debug("io_stats || Process has already been killed")

        else:
            self.main_logger.debug("io_stats || Platform not supported for this operation")
            return False

        self.main_logger.debug("io_stats || completed")
        return io_stats
