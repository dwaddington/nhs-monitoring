import os
import sys
import re
import subprocess
import platform
import logging
import logging.handlers

python_version = platform.python_version_tuple()


class cpustats(object):
    def __init__(self, agent_config, internal_config, main_logger):
        self.agent_config = agent_config
        self.internal_config = internal_config
        self.main_logger = main_logger
        self.os = None
        self.proc_fs_loc = None

    def run(self):
        self.main_logger.debug("cpu_statistics || start")

        cpu_stats = {}

        if sys.platform == "linux2":
            self.main_logger.debug("cpu_statistics || linux2")

            head_regex_pattern = re.compile(r'.*?([%][a-zA-Z0-9]+)[\s+]?')
            item_regex_pattern = re.compile(r'.*?\s+(\d+)[\s+]?')
            value_regex_pattern = re.compile(r'\d+\.\d+')

            try:
                proc = subprocess.Popen(["mpstat", "-P", "ALL", "1", "1"], stdout=subprocess.PIPE, close_fds=True)
                stats = proc.communicate()[0]

                if int(python_version[1]) >= 6:
                    try:
                        proc.kill()
                    except Exception, e:
                        self.main_logger.debug("cpu_statistics || Process has already been killed")

                stats = stats.split("\n")
                header = stats[2]
                header_names = re.findall(head_regex_pattern, header)
                device = None

                for s_i in range(4, len(stats)):
                    row = stats[s_i]

                    if not row:
                        break

                    dev_match = re.match(item_regex_pattern, row)

                    if dev_match is not None:
                        device = "CPU%s" % dev_match.groups()[0]

                    values = re.findall(value_regex_pattern, row.replace(",", "."))

                    cpu_stats[device] = {}

                    for h_i in range(0, len(header_names)):
                        header_name = header_names[h_i]
                        cpu_stats[device][header_name] = values[h_i]

            except OSError, e:
                return False

            except Exception, e:
                if int(python_version[1]) >= 6:
                    try:
                        if proc:
                            proc.kill()
                    except UnboundLocalError, e:
                        self.main_logger.debug("Process has already been killed")
                    except Exception, e:
                        self.main_logger.debug("Process has already been killed")

                import traceback
                self.main_logger.error("cpu_statistics || exception: %s", traceback.format_exc())
                return False

        else:
            self.main_logger.debug("cpu_statistics || unsupported platform")
            return False

        self.main_logger.debug("cpu_statistics || completed")
        return cpu_stats
