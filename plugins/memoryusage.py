import sys
import re


class memoryusage(object):
    def __init__(self, agent_config, internal_config, main_logger):
        self.agent_config = agent_config
        self.internal_config = internal_config
        self.main_logger = main_logger

    def run(self):
        self.main_logger.debug("memory_usage || start")

        if sys.platform == "linux2":
            self.main_logger.debug("memory_usage || linux 2")

            try:
                self.main_logger.debug("memory_usage || Attempting open")

                mem_info_proc = open("/proc/meminfo", "r")
                lines = mem_info_proc.readlines()

            except IOError, e:
                self.main_logger.debug("memory_usage || exception %s", e)
                return False

            self.main_logger.debug("memory_usage || open was a success, parsing....")

            mem_info_proc.close()

            reg_expression = re.compile(r'([0-9]+)')

            memory_info = {}

            self.main_logger.debug("memory_usage || parsing... going into loop")

            for line in lines:
                values = line.split(":")
                try:
                    match = re.search(reg_expression, values[1])
                    if match is not None:
                        memory_info[str(values[0])] = match.group(0)

                except IndexError:
                    break

            self.main_logger.debug("memory_usage || loop done")

            memory_data = {}

            memory_data["physical_free"] = 0
            memory_data["physical_used"] = 0
            memory_data["cached"] = 0
            memory_data["swap_free"] = 0
            memory_data["swap_used"] = 0

            try:
                self.main_logger.debug("memory_usage || formatting physical memory data")

                physical_total = float(memory_info["MemTotal"])
                physical_free = float(memory_info["MemFree"])
                physical_used = physical_total - physical_free

                memory_data["physical_free"] = physical_free / 1024
                memory_data["physical_used"] = physical_used / 1024
                memory_data["cached"] = float(memory_info["Cached"]) / 1024

            except IndexError:
                self.main_logger.debug("memory_usage || formatting physical memory data - IndexError")

            except KeyError:
                self.main_logger.debug("memory_usage || formatting physical memory data - KeyError")

            self.main_logger.debug("memory_usage || formatted physical memory")

            try:
                self.main_logger.debug("memory_usage || formatting swap memory data")

                swap_total = float(memory_info["SwapTotal"])
                swap_free = float(memory_info["SwapFree"])
                swap_used = swap_total - swap_free

                memory_data["swap_free"] = swap_free / 1024
                memory_data["swap_used"] = swap_used / 1024

            except IndexError:
                self.main_logger.debug("memory_usage || formatting swap memory data - IndexError")

            except KeyError:
                self.main_logger.debug("memory_usage || formatting swap memory data - KeyError")

            self.main_logger.debug("memory_usage || formatted, completed")
            return memory_data

        else:
            self.main_logger.debug("memory_usage || unsupported platform")
            return False
