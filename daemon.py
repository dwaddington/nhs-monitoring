import atexit
import os
import sys
import time

from signal import SIGTERM


class Daemon:
    def __init__(self, pidfile, stdin=os.devnull, stdout=os.devnull, stderr=os.devnull):
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pidfile = pidfile

    def conv_to_daemon(self):
        try:
            pid = os.fork()
            if pid > 0:
                sys.exit(0)

        except OSError, e:
            sys.stderr.write("Fork #1 faild %d (%s) \n"
                             % (e.errno, e.strerror))
            sys.exit(1)

        os.chdir("/")
        os.setsid()
        os.umask(0)

        try:
            pid = os.fork()
            if pid > 0:
                sys.exit(0)
        except OSError, e:
            sys.stderr.write("Fork #2 failed %d (%s) \n"
                             % (e.errno, e.strerror))
            sys.exit(1)

        if not sys.platform == "darwin":
            sys.stdout.flush()
            sys.stderr.flush()
            s_in = file(self.stdin, "r")
            s_out = file(self.stdout, "a+")
            s_err = file(self.stderr, "a+", 0)
            os.dup2(s_in.fileno(), sys.stdin.fileno())
            os.dup2(s_out.fileno(), sys.stdout.fileno())
            os.dup2(s_err.fileno(), sys.stderr.fileno())

        atexit.register(self.delpid)
        pid = str(os.getpid())
        file(self.pidfile, "w+").write("%s\n" % pid)

    def delpid(self):
        os.remove(self.pidfile)

    def start(self):
        print "Starting..."
        try:
            p_file = file(self.pidfile, "r")
            pid = int(p_file.read().strip())
            p_file.close()
        except IOError:
            pid = None
        except SystemExit:
            pid = None

        if pid:
            message = "The pidfile %s already exists. Is there another daemon running?"
            sys.stderr.write(message % self.pidfile)
            sys.exit(1)

        self.conv_to_daemon()
        self.run()

    def stop(self):
        print "Stopping..."

        try:
            p_file = file(self.pidfile, "r")
            pid = int(p_file.read().strip())
            p_file.close()
        except IOError:
            pid = None
        except SystemExit:
            pid = None

        if not pid:
            message = "The pidfile %s already exists. Is there another daemon running?"
            sys.stderr.write(message % self.pidfile)

            if os.path.exists(self.pidfile):
                os.remove(self.pidfile)

            return

        try:
            while True:
                os.kill(pid, SIGTERM)
                time.sleep(0.1)
        except OSError, err:
            err = str(err)
            if err.find("No process found") > 0:
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                print str(err)
                sys.exit(1)

        print "Stopped"

    def restart(self):
        self.stop()
        self.start()

    def run(self):
        """
            Overridden by subclasses
        """
