import os
import sys


class pluginManager:
    def __init__(self, agent_config, internal_config, main_logger):
        self.agent_config = agent_config
        self.internal_config = internal_config
        self.main_logger = main_logger
        self.plugins = None

    def load_plugins(self):
        self.main_logger.debug("get_plugins || start")

        if "plugin_dir" in self.agent_config and self.agent_config["plugin_dir"] != "":
            self.main_logger.info("get_plugins || Loading from directory: %s", self.agent_config["plugin_dir"])
            if os.access(self.agent_config["plugin_dir"], os.R_OK) is not True:
                self.main_logger.warning("get_plugins || Plugin directory set but not readable")
                self.main_logger.warning("get_plugins || Skipping the loading of plugins...")
                return False
        else:
            self.main_logger.debug("get_plugins || Plugin directory not set")
            return False

        if self.plugins is None:
            self.main_logger.debug("get_plugins || Initial plugin load from %s", self.agent_config["plugin_dir"])
            sys.path.append(self.agent_config["plugin_dir"])

            self.plugins = []
            plugins = []

            for root, dirs, files in os.walk(self.agent_config["plugin_dir"]):
                for name in files:
                    self.main_logger.debug("get_plugins || Found: %s", name)
                    name = name.split(".", 1)
                    try:
                        if name[1] == "py":
                            self.main_logger.debug("get_plugins ||" + name[0] + "." + name[1] + " is a plugin")
                            plugins.append(name[0])
                    except IndexError, e:
                        continue

            for plugin_name in plugins:
                self.main_logger.debug("get_plugins || Loading: %s", plugin_name)
                p_path = os.path.join(self.agent_config["plugin_dir"], "%s.py" % plugin_name)

                if os.access(p_path, os.R_OK) is not True:
                    self.main_logger.error("get_plugins || Failed to read %s", p_path)
                    self.main_logger.error("get_plugins || Plugin skipped")
                    continue

                try:
                    import imp
                    imported_plugin = imp.load_source(plugin_name, p_path)
                    self.main_logger.debug("get_plugins || Imported: %s", plugin_name)

                    plugin_class = getattr(imported_plugin, plugin_name)

                    try:
                        plugin_object = plugin_class(self.agent_config, self.internal_config, self.main_logger)
                    except TypeError:
                        try:
                            plugin_object = plugin_class(self.agent_config, self.main_logger)
                        except TypeError:
                            plugin_object = plugin_class()

                    self.main_logger.debug("get_plugins || Finished loading %s", plugin_name)
                    self.plugins.append(plugin_object)
                except Exception, ex:
                    import traceback
                    self.main_logger.error("get_plugins || Exception caught for: %s", plugin_name)
                    self.main_logger.error("get_plugins || Exception trace: %s", traceback.format_exc())

        if self.plugins is not None:
            self.main_logger.debug("get_plugins || Executing plugins")
            output = {}
            for plugin in self.plugins:
                self.main_logger.info("get_plugins || Executing %s", plugin.__class__.__name__)

                try:
                    output[plugin.__class__.__name__] = plugin.run()
                except Exception, ex:
                    import traceback
                    self.main_logger.error("get_plugins || Exception in %s", plugin.__class__.__name__)
                    self.main_logger.error("get_plugins || Exception trace: %s", traceback.format_exc())

                self.main_logger.debug("get_plugins || %s output: %s", plugin.__class__.__name__, output[plugin.__class__.__name__])
                self.main_logger.info("get_plugins || Executed: %s", plugin.__class__.__name__)

            self.main_logger.debug("get_plugins || returning")
            return output
        else:
            self.main_logger.debug("get_plugins || no plugins found")
            return False
