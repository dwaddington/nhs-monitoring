import logging
import logging.handlers
import sys

agent_config = {}
agent_config['log_level'] = logging.INFO
agent_config['check_interval'] = 60
agent_config['version'] = "0.01"
agent_config['tmp_directory'] = "/tmp/"
agent_config['pid_directory'] = agent_config['tmp_directory']

internal_config = {}

if int(sys.version_info[1]) <= 3:
    print "Old version of python detected"
    sys.exit(1)

import ConfigParser
import glob
import subprocess
import os
import json
from hashlib import md5

from daemon import Daemon
from pluginManager import pluginManager

try:
    path = os.path.realpath(__file__)
    path = os.path.dirname(path)

    config = ConfigParser.ConfigParser()

    config_path = path + "/config.cfg"

    if not os.access(config_path, os.R_OK):
        print "Unable to read configuration file at " + config_path
        sys.exit(1)

    if os.path.isdir(config_path):
        for config_file in glob.glob(os.path.join(config_path, "*.cfg")):
            config.read(config_file)
    else:
        config.read(config_path)

    plugin_path = path + "/plugins/"
    if os.path.isdir(plugin_path):
        agent_config["plugin_dir"] = plugin_path
    else:
        print "Unable to execute program as there are no plugins to load"
        sys.exit(1)

    if config.has_option("Main", "logging_level"):
        level_map = {
            "critical": logging.CRITICAL,
            "debug": logging.DEBUG,
            "error": logging.ERROR,
            "fatal": logging.FATAL,
            "info": logging.INFO,
            "warn": logging.WARN,
            "warning": logging.WARNING
        }

        custom_log_level = config.get("Main", "logging_level")

        try:
            agent_config["log_level"] = level_map[custom_log_level.lower()]
        except KeyError, ex:
            agent_config["log_level"] = logging.INFO

except ConfigParser.NoSectionError, e:
    print "Config file nor propertly formatted or not found"
    sys.exit(1)

except ConfigParser.ParsingError, e:
    print "Config file nor propertly formatted or not found"
    sys.exit(1)

for section in config.sections():
    internal_config[section] = {}

    for option in config.options(section):
        internal_config[section][option] = config.get(section, option)


class agent(Daemon):
    def run(self):
        main_logger.debug("Collecting basic system information")

        import platform
        system_info = {
            "machine": platform.machine(),
            "platform": sys.platform,
            "processor": platform.processor(),
            "python_version": platform.python_version(),
            "Proc Cores": self.get_num_cores()
        }

        main_logger.info("System: " + str(system_info))
        plugins = pluginManager(agent_config, internal_config, main_logger)
        testing = plugins.load_plugins()
        payload = json.dumps(testing, encoding="latin-1").encode("utf-8")
        payload_hash = md5(payload).hexdigest()
        main_logger.debug("Payload: %s", payload)
        main_logger.debug("Payload Hash: %s", payload_hash)

    def get_num_cores(self):
        if sys.platform == "linux2":
            grep_proc = subprocess.Popen(
                ["grep", "model name", "/proc/cpuinfo"],
                stdout=subprocess.PIPE,
                close_fds=True)
            wc_grep = subprocess.Popen(
                ["wc", "-l"],
                stdin=grep_proc.stdout,
                stdout=subprocess.PIPE,
                close_fds=True)
            output = wc_grep.communicate()[0]
            return int(output)

if __name__ == "__main__":

    log_file = os.path.join(agent_config["tmp_directory"], "agent.log")
    if not os.access(agent_config["tmp_directory"], os.W_OK):
        print "Unable to write to log file at " + log_file
        sys.exit(1)

    handler = logging.handlers.RotatingFileHandler(
        log_file,
        maxBytes=10485760,
        backupCount=5)
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    handler.setFormatter(formatter)

    main_logger = logging.getLogger("main")
    main_logger.setLevel(agent_config["log_level"])
    main_logger.addHandler(handler)

    main_logger.info("---")
    main_logger.info("Agent %s started", agent_config["version"])
    main_logger.info("---")

    pidFile = os.path.join(agent_config["pid_directory"], "agent.pid")

    if not os.access(agent_config["pid_directory"], os.W_OK):
        print "Unable to write to the PID file: " + pidFile
        sys.exit(1)

    main_logger.info("PID for process: %s", pidFile)

    test = agent(pidFile)
    test.start()
    test.stop()
